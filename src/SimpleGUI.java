import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Madi on 16.10.2016.
 */
public class SimpleGUI extends JFrame {

    private JButton button = new JButton("Санау");
    private JTextField inputA = new JTextField("", 5);
    private JTextField inputB = new JTextField("", 5);
    private JLabel labelA = new JLabel("а:");
    private JLabel labelB = new JLabel("b:");


    public SimpleGUI() {
        super("Simple Example");
        this.setBounds(100, 100, 250, 100);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Container container = this.getContentPane();
        container.setLayout(new GridLayout(3, 2, 2, 2));

        container.add(labelA);
        container.add(inputA);
        container.add(labelB);
        container.add(inputB);

        button.addActionListener(new ButtonEventListener());
        container.add(button);
    }

    class ButtonEventListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String message = "";
            message += "Санау басталды\n";
            message += "a=" + inputA.getText() + "\n";
            message += "b=" + inputB.getText() + "\n";
            if (Integer.parseInt(inputB.getText()) == 0) {
                message += "B саны нольге тен болуы мумкин емес";
            } else {
                String result = systemNumber(Integer.parseInt(inputA.getText()), Integer.parseInt(inputB.getText()));
                message += result;
            }


            JOptionPane.showMessageDialog(null,
                    message,
                    "Output",
                    JOptionPane.PLAIN_MESSAGE);
        }
    }

    public String systemNumber(int a, int b) {
        String message = "";
        int r = 0;
        int q = 1;
        int t = 1;
        if (a > 0) {
            if (b > 0) {
                System.out.println("a>0;b>0");
                while (t < a) {
                    t = b * q;
                    message += b + " *" + q + "=" + t + "\n";
                    System.out.println(message);
                    q += 1;
                }
                r = a - (q - 2) * b;
                message += "q=" + (q - 2) + "\n";
                message += "r=" + r + "\n";
            } else if (b < 0) {
                System.out.println("a>0;b<0");
                while (t > -a) {
                    t = b * q;
                    message += b + " *" + q + "=" + t + "\n";
                    System.out.println(message);
                    q += 1;
                }
                r = -a - (q - 2) * b;
                message += "q=-" + (q - 2) + "\n";
                message += "r=" + -r + "\n";

            }

        } else if (a < 0) {
            if (b > 0) {
                System.out.println("a<0;b>0");
                while (-t < -a) {
                    t = b * -q;
                    message += b + " *" + -q + "=" + t + "\n";
                    System.out.println(message);
                    q += 1;
                }
                r = a-(-(q-1))*b ;
                message += "q=-" + (q - 1) + "\n";
                message += "r=" + r + "\n";

            } else if (b < 0) {
                System.out.println("a<0;b<0");
                while (t > a) {
                    t = b * q;
                    message += b + " *" + q + "=" + t + "\n";
                    System.out.println(message);
                    q += 1;
                }
                r =  a - (q - 1) * b ;
                message += "q=" + (q - 1) + "\n";
                message += "r=" + r + "\n";

            }
        } else {
            message += "q=" + 0 + "\n";
            message += "r=" + 0 + "\n";
        }

        return message;
    }

}
